tailwind.config = {
  theme: {
    extend: {
      fontFamily: {
        sans: ['DynaPuff', ...tailwind.defaultTheme.fontFamily.sans],
      },
      fontSize: {
        '4xs': '.25rem',
        '3xs': '.5rem',
        'xxs': '.625rem',
      },
      colors: {
        'green-main': '#486d5d',
        'green-dark': '#2e5142',
        'green-light': '#94c399',
      }
    }
  }
};

let emulated = {
  detail: {
    listener: "message", 
    event: {
      service: "twitch",
      data: {
        time: Date.now(),
        tags: {
          "badge-info": "",
          badges: "moderator/1,partner/1",
          color: "#5B99FF",
          "display-name": "StreamElements",
          emotes: "25:46-50",
          flags: "",
          id: "43285909-412c-4eee-b80d-89f72ba53142",
          mod: "1",
          "room-id": "85827806",
          subscriber: "0",
          "tmi-sent-ts": "1579444549265",
          turbo: "0",
          "user-id": "100135110",
          "user-type": "mod"
        },
        nick: "WWWWWWWWWWWWWWWWWWWWWWWWW",
        userId: "100135110",
        displayName: "WWWWWWWWWWWWWWWWWWWWWWWWW",
        displayColor: "#5B99FF",
        badges: [{
          type: "moderator",
          version: "1",
          url: "https://static-cdn.jtvnw.net/badges/v1/3267646d-33f0-4b17-b3df-f923a41db1d0/3",
          description: "Moderator"
        }, {
          type: "partner",
          version: "1",
          url: "https://static-cdn.jtvnw.net/badges/v1/d12a2e27-16f6-41d0-ab77-b780518f00a3/3",
          description: "Verified"
        }],
        channel: "WWWWWWWWWWWWWWWWWWWWWWWWW",
        // text: "Howdy! My name is Bill and I am here to serve Kappa",
        text: "asdfasdf",
        isAction: !1,
        emotes: [{
          type: "twitch",
          name: "Kappa",
          id: "25",
          gif: !1,
          urls: {
            1: "https://static-cdn.jtvnw.net/emoticons/v1/25/1.0",
            2: "https://static-cdn.jtvnw.net/emoticons/v1/25/1.0",
            4: "https://static-cdn.jtvnw.net/emoticons/v1/25/3.0"
          },
          start: 46,
          end: 50
        }],
        msgId: Math.random(),
      },
      // renderedText: 'Howdy! My name is Bill and I am here to serve <img src="https://static-cdn.jtvnw.net/emoticons/v1/25/1.0" srcset="https://static-cdn.jtvnw.net/emoticons/v1/25/1.0 1x, https://static-cdn.jtvnw.net/emoticons/v1/25/1.0 2x, https://static-cdn.jtvnw.net/emoticons/v1/25/3.0 4x" title="Kappa" class="emote">'
      renderedText: "Lorem ipsum dolor sit amet,",
    }
  }
};

const CustomChat = {
  props: {
  	displayName: String,
    renderedText: String,
    position: Object,
  },
  computed: {
    styledRenderedText() {
      const stylesAdded = this.renderedText.replaceAll('<img ', '<img class="inline w-auto h-[1.5rem]" ');
      
      return stylesAdded;
    },
    usernameTextSize() {
      if (this.displayName.length > 18) {
        return "text-xs";
      }
      
      return "text-md";
    },
  },
  data() {
    return {
      myText: this.text, 
    };
  },
  template: `
      <section
        class="relative flex justify-center mx-2 mt-6 mb-2 min-w-[200px] max-w-[85%] text-md"
        :class="[position.alignSelf, position.bounceDirection]"
      >
        <div
          id="msg-username"
          class="absolute -top-5 py-0.5 px-2 rounded-lg bg-green-dark text-neutral-100"
          :class="[position.absolute, usernameTextSize]"
        >
          {{ displayName }}
        </div>
        <div
          id="msg-rendered-text"
          class="inline px-4 py-2 w-full bg-green-light rounded-xl leading-6 text-neutral-800"
          v-html="styledRenderedText"
        ></div>
      </section>
  `,
};

const app = Vue.createApp({
  template: `
	<TransitionGroup
      enter-active-class="animate__animated"
	  leave-active-class="animate__animated animate__fadeOut"
    >
      <CustomChat
        v-for="{ badges, displayName, emotes, msgId, position, renderedText, service, userId } in messages"
        :displayName="displayName"
        :renderedText="renderedText"
        :position="position"
	    :key="msgId"
	  >
      </CustomChat>
	</TransitionGroup>
  `,
  data() {
    return {
      messages: [],
    };
  },
  methods: {
  	handleEvent(event) {
      event.preventDefault();
      if (event.detail.event.listener === 'widget-button') {
        if (event.detail.event.field === 'testMessage') {
          const newMsgId = Math.random();
          const newEmulated = JSON.parse(JSON.stringify(emulated));
          newEmulated.detail.event.data.msgId = newMsgId;
          window.dispatchEvent(new CustomEvent("onEventReceived", newEmulated));
        }

        return;
      }
      
      const listener = event?.detail?.listener;
      if (listener === 'message') {
      	const sentMessage = event?.detail?.event;
      	this.handleMessage(sentMessage);
      }
    },
    randomizePosition() {
	  const rng = Math.random() * 2;
      
      if (rng >= 1) {
        return { alignSelf: "self-end", absolute: "-left-6", bounceDirection: "animate__bounceInRight" };
      }

      return { alignSelf: "self-start", absolute: "-right-6", bounceDirection: "animate__bounceInLeft" };
    },
    pop() {
	  this.messages.splice(0, 1);
    },
    push(newItem) {
      this.messages.splice(this.messages.length, 0, newItem); 
    },
    handleMessage(msg) {
      const {
        data: {
          badges,
          displayName,
          emotes,
          msgId,
          userId,
        },
        renderedText,
        service,
      } = msg;
    
      const newMsg = {
        badges,
        displayName,
        emotes,
        msgId,
        renderedText,
        service,
        userId,
    	position: this.randomizePosition(),
      };
      
      if (this.messages.length >= 5) {
        this.pop();
      }

	  this.push(newMsg);
    },
  },
  mounted: function () {
  	window.addEventListener('onWidgetLoad', function () {});
    window.addEventListener('onEventReceived', this.handleEvent);
  },
  unmounted: function () {
    window.removeEventListener('onWidgetLoad', function () {});
    window.removeEventListener('onEventReceived', this.handleEvent);
  },
});

app.component('CustomChat', CustomChat);

app.mount('#app');